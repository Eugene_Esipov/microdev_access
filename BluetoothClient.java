public class BluetoothClientImpl implements BluetoothClient {

    @Override
    public Completable connectToServer(@NonNull BluetoothDevice device, @NonNull UUID uuid) {
        return createSocket(device, uuid)
                .andThen(connectToSocket())
                .andThen(startMessageReceiver(device.getAddress()));
    }

    @NotNull
    private Completable startMessageReceiver(@NonNull String mac) {
        return Completable.fromAction(() -> subscribeMessageReceiver(mac));
    }

    private void subscribeMessageReceiver(@NonNull String mac) {
        if (inputStreamDisposable == null) {
            inputStreamDisposable = Observable.create(this::readMessageInputStream)
                    .subscribe(message -> publishDeviceState(new BluetoothClientOnMessageReceiveNextImpl(message, mac)),
                            throwable -> publishDeviceState(new BluetoothClientOnMessageReceiveErrorImpl(throwable)));
        }
    }

    private void readMessageInputStream(@NonNull ObservableEmitter<String> emitter) {
        while (!emitter.isDisposed()) {
            try {
                int bytesAvailable = inputStream.available();
                if (bytesAvailable > 0) {
                    byte[] buffer = new byte[bytesAvailable];
                    int readBytes = inputStream.read(buffer);
                    emitter.onNext(new String(buffer));
                }
            } catch (IOException e) {
                emitter.onError(e);
                break;
            }
        }
    }

}

